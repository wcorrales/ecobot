categories = {
    "food": "Alimento",
    "drink": "Bebida",
    "plant": "Planta",
    "input": "Insumo",
    "home": "Hogar",
    "personal": "Personal",
    "service": "Servicio",
    "other": "Otro",
}

buttons = {
    "accept": "Aceptar",
    "cancel": "Cancelar",
    "grade": "Calificar",
    "note": "Nota",
    "contact": "Enviar Contacto",
    "payment": "Realizar Pago",
}

commands = {
    "balance": "saldo",
    "check": "verificacion",
    "check_columns": ["Miembros", "Transacciones", "Saldos Positivos", "Saldos Negativos"],
    "check_title": "Balance Comprobacion",
    "check_file": "balance",
    "directory": "directorio",
    "directory_columns": ["Categoria", "Producto", "Precio", "Productor", "Transacciones", "Calificacion"],
    "directory_title": "Directorio Productos",
    "directory_file": "directorio",
    "help_text": (
        "Comandos disponibles:\n"
        + "/saldo - saldo billetera virtual\n"
        + "/verificacion - integridad sistema\n"
        + "/directorio - bienes o servicios\n"
        + "/help - ayuda\n"
        + "\n"
        + "Como Funciona:\n"
        + "1. Realizar busqueda por:\n"
        + "   - vendedor\n"
        + "   - categoria (alimento, bebida, planta, insumo, hogar, personal, servicio, otro)\n"
        + "   - item (producto o servicio)\n"
        + "2. Se puede chatear con vendedor\n"
        + "3. Se realiza pago\n"
    ),
}

messages = {
    "amount": "MONTO",
    "client": "Cliente",
    "seller": "Vendedor",
    "error_amount": "MONTO mayor a saldo de GARANTIA!",
    "error_contact": "Error en verificacion contacto\ntelefono no autorizado\ncrear cuenta en ecopz.com!",
    "error_number": "Error: numero mal escrito!",
    "error_result": "No se encontraron resultados!",
    "request_contact": "Haga click en: Enviar Contacto!",
    "request_amount": "Por favor escribir MONTO:",
    "request_grade": "Por favor escribir CALIFICACION(1-5):",
    "request_note": "Por favor escribir NOTA:",
    "status_admitted": "Transaccion CONFIRMADA!",
    "status_admitted_done": "Transaccion esta CONFIRMADA!",
    "status_balance": "SALDO actual: %s!",
    "status_balance_get": "ver estado de cuenta en /saldo",
    "status_cancelled": "Transaccion CANCELADA!",
    "status_cancelled_done": "Transaccion esta CANCELADA!",
    "status_contact_done": "Verificacion contacto COMPLETA!",
    "status_received": "Transferencia RECIBIDA!",
    "status_rejected": "Transaccion RECHAZADA!",
    "status_send": "Transferencia REALIZADA!",
    "status_send_grade": "Calificacion REALIZADA!",
    "status_send_note": "Nota INGRESADA!",
}
