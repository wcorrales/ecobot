#!/usr/bin/env python3
import logging
from asyncio import run
from json import dumps
from re import sub

from aiogram import Bot, Dispatcher, F, Router
from aiogram.enums import ParseMode
from aiogram.filters import Command
from aiogram.types import (
    BufferedInputFile,
    CallbackQuery,
    InlineKeyboardButton,
    InlineKeyboardMarkup,
    KeyboardButton,
    Message,
    ReplyKeyboardMarkup,
    ReplyKeyboardRemove,
)
from duckdb import connect
from duckdb_cursor import CURSOR

from config import AppConfig
from dicts import categories, buttons, commands, messages
from utils import escape, unescape, thous, set_table, set_pdf, set_logging


router = Router()
dp = Dispatcher()
dp.include_router(router)
bot = Bot(token=AppConfig.BOT_TOKEN)
steps = {}  # cid: transaction, note, grade


set_logging(AppConfig.LOG_LEVEL, log_file=f"{AppConfig.APP_PATH}ecobot.log")


def get_user_id(cur, cid):
    row = cur.get("SELECT id FROM workers WHERE bot=?;", [cid], one=True)
    return -1 if not row else row[0]


def set_user_id(cur, cid, phone):
    logging.warning(f"set_user_id: {cid}, phone: {phone}")
    row = cur.get("UPDATE workers SET bot=? WHERE phone=? RETURNING id;", [cid, "+" + sub("\D", "", phone)], one=True)
    return -1 if not row else row[0]


def set_user_resumen(cur, w):
    cur.set(
        """UPDATE workers w
           SET income = COALESCE(tx_in, 0), outcome = COALESCE(tx_out, 0), transactions = tx
           FROM (SELECT ? AS w_id, COUNT(*) AS tx,
                        SUM(total) FILTER (WHERE worker_id=?) AS tx_in,
                        SUM(total) FILTER (WHERE client_id=?) AS tx_out
                 FROM transactions
                 WHERE admitted AND (worker_id=? OR client_id=?)) t
           WHERE w.id=t.w_id;""",
        [w, w, w, w, w],
    )


async def request_contact(cid):
    logging.warning("request_contact")
    markup = ReplyKeyboardMarkup(
        one_time_keyboard=True,
        keyboard=[[KeyboardButton(text=buttons["contact"], request_contact=True)]],
    )
    await bot.send_message(cid, messages["request_contact"], reply_markup=markup)


@router.message(Command("start", "help", commands["balance"], commands["check"], commands["directory"]))
async def commands_handler(m: Message) -> None:
    logging.debug("commands_handler")
    cid = m.chat.id
    await bot.send_chat_action(cid, "typing")  # await ChatActions.typing()
    command = m.text
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    user_id = get_user_id(cur, cid)
    if user_id < 0:
        await request_contact(cid)
    else:
        if cid in steps:
            del steps[cid]
        if m.text not in ["/start", "/help"]:
            if command == ("/" + commands["balance"]):
                row = cur.get("SELECT income - outcome FROM workers WHERE id=?;", [user_id], one=True)
                await bot.send_message(cid, messages["status_balance"] % thous(round(row[0], 2)))
            elif command == ("/" + commands["check"]):
                rows = cur.get(
                    """WITH balances AS (SELECT worker, transactions, balance
                                         FROM workers,
                                         LATERAL(SELECT income - outcome AS balance) x
                                         WHERE balance <> 0)
                       SELECT worker, transactions,
                              CASE WHEN balance > 0 THEN balance ELSE 0 END AS incomes,
                              CASE WHEN balance < 0 THEN balance ELSE 0 END AS outcomes
                       FROM balances
                       ORDER BY worker;"""
                )
                t_rows = []
                txs = pos = neg = 0
                for r in rows:
                    txs = txs + r[1]
                    pos = pos + r[2]
                    neg = neg + r[3]
                    t_rows.append([unescape(r[0]), thous(r[1]), thous(round(r[2], 2)), thous(round(r[3], 2))])
                table = set_table(
                    commands["check_columns"],
                    t_rows,
                    [len(rows), txs, pos, neg],
                    justify=["l", "r", "r", "r"],
                    widths=[30, 15, 15, 15],
                )
                await bot.send_document(
                    cid,
                    BufferedInputFile(
                        set_pdf(commands["check_title"], table), filename=f'{commands["check_file"]}.pdf'
                    ),
                )
            elif command == ("/" + commands["directory"]):
                rows = cur.get(
                    """WITH categories AS (SELECT ?::JSON AS x)
                       SELECT (SELECT x->>p.category FROM categories), product, ROUND(price,2) || '/' || tag,
                              worker, transactions, quality
                       FROM products p,
                       LATERAL(SELECT worker, transactions, ROUND(COALESCE(quality, 0), 2) AS quality
                               FROM workers WHERE id=p.worker_id AND bot IS NOT NULL) w
                       ORDER BY product;""",
                    [dumps(categories)],
                )
                table = set_table(
                    commands["directory_columns"],
                    [(r[0], unescape(r[1]), r[2], unescape(r[3]), thous(r[4]), r[5]) for r in rows],
                    justify=["l", "l", "r", "l", "r", "r"],
                    widths=[11, 25, 12, 12, 7, 7],
                )
                await bot.send_document(
                    cid,
                    BufferedInputFile(
                        set_pdf(commands["directory_title"], table), filename=f'{commands["directory_file"]}.pdf'
                    ),
                )
        else:
            await bot.send_message(cid, commands["help_text"])
    del cur


@router.message(
    F.content_type.in_({"contact", "audio", "document", "location", "photo", "sticker", "video", "video_note", "voice"})
)
async def contact_handler(m: Message) -> None:
    logging.debug("contact_handler")
    cid = m.chat.id
    await bot.send_chat_action(cid, "typing")  # await ChatActions.typing()
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    user_id = get_user_id(cur, cid)
    if user_id < 0:
        logging.warning(m.content_type)
        if m.content_type != "contact":
            await request_contact(cid)
        else:
            user_id = set_user_id(cur, cid, m.contact.phone_number)
            if user_id < 0:
                await bot.send_message(cid, messages["error_contact"])
            else:
                await bot.send_message(cid, messages["status_contact_done"], reply_markup=ReplyKeyboardRemove())
    else:
        if cid in steps:
            del steps[cid]
        await bot.send_message(cid, messages["error_result"])
    del cur


@router.message(F.text)
async def message_handler(m: Message) -> None:
    logging.debug("message_handler")
    cid = m.chat.id
    await bot.send_chat_action(cid, "typing")  # await ChatActions.typing()
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    user_id = get_user_id(cur, cid)
    if user_id < 0:
        await request_contact(cid)
    elif cid in steps:
        if "transaction" in steps[cid]:
            if m.text == "" or sub("^\d+(\.\d+)?$", "", m.text) != "":  # /\d+(,\d+)*(\.\d+)?$/
                await bot.send_message(cid, messages["error_number"])
            else:
                product_id = sub("transaction_", "", steps[cid])
                user_name = (cur.get("SELECT worker FROM workers WHERE id=?;", [user_id], one=True))[0]
                item = cur.get(
                    """SELECT id, worker_id, category, product, price, tag,
                              worker, bot, NEXTVAL('transactions_id_seq') AS tx_id
                       FROM products p,
                       LATERAL(SELECT worker, bot FROM workers WHERE id=p.worker_id) w
                       WHERE id=?;""",
                    [product_id],
                    one=True,
                    as_dict=True,
                )
                cur.set(
                    """INSERT INTO transactions
                       VALUES(?, ?, ?,
                              (SELECT category FROM products WHERE id=?),
                              (SELECT product FROM products WHERE id=?),
                              ?, FALSE, FALSE, FALSE, 2, NULL, NULL, NULL, NULL, NOW(),
                              NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
                       RETURNING id;""",
                    [item["tx_id"], item["worker_id"], user_id, product_id, product_id, m.text],
                )
                self_text = f'{messages["status_send"]}\n\n'
                self_text += f'{messages["seller"]}: [{item["worker"]}](tg://user?id={item["bot"]})\n'
                self_text += f'{categories[item["category"]]} - {unescape(item["product"])}\n'
                self_text += f'{messages["amount"]}: {m.text}'
                self_markup = InlineKeyboardMarkup(
                    inline_keyboard=[[InlineKeyboardButton(text="Cancelar", callback_data=f'cancel_{item["tx_id"]}')]]
                )
                await bot.send_message(cid, self_text, parse_mode=ParseMode.MARKDOWN, reply_markup=self_markup)
                other_text = f'{messages["status_received"]}\n\n'
                other_text += f'{messages["client"]}: [{user_name}](tg://user?id={cid})\n'
                other_text += f'{categories[item["category"]]} - {unescape(item["product"])}\n'
                other_text += f'{messages["amount"]}: {m.text}'
                other_markup = InlineKeyboardMarkup(
                    inline_keyboard=[
                        [
                            InlineKeyboardButton(text=buttons["accept"], callback_data=f'confirm_{item["tx_id"]}'),
                            InlineKeyboardButton(text=buttons["cancel"], callback_data=f'cancel_{item["tx_id"]}'),
                        ]
                    ]
                )
                await bot.send_message(
                    item["bot"], other_text, parse_mode=ParseMode.MARKDOWN, reply_markup=other_markup
                )
        # elif 'note' in steps[cid]:
        #   tx_id = sub('nota_', '', steps[cid])
        #   cur.set("UPDATE transactions SET note=? WHERE id=?;", [m.text, tx_id])
        #   await bot.send_message(cid, messages['status_send_note'])
        elif "grade" in steps[cid]:
            if m.text == "" or sub("[1-5]", "", m.text) != "":
                await bot.send_message(cid, messages["error_number"])
            else:
                tx_id = sub("grade_", "", steps[cid])
                cur.set("UPDATE transactions SET quality=?, service=? WHERE id=?;", [m.text, m.text, tx_id])
                cur.set(
                    """UPDATE workers w
                       SET service = COALESCE(avg_service, 0), quality = COALESCE(avg_quality, 0)
                       FROM (SELECT worker_id, AVG(service) AS avg_service, AVG(quality) AS avg_quality
                             FROM transactions WHERE admitted AND client_id=? GROUP BY worker_id) t
                       WHERE w.id=t.worker_id;""",
                    [user_id],
                )
                await bot.send_message(cid, messages["status_send_grade"])
        del steps[cid]
    else:
        rows = cur.get(
            """WITH categories AS (SELECT ?::JSON AS x)
               SELECT id, worker, category, product, price, tag, bot, transactions, quality
               FROM products p,
               LATERAL(SELECT worker, bot, transactions, ROUND(COALESCE(quality, 0), 2) AS quality
                       FROM workers WHERE id=p.worker_id AND id<>? AND bot IS NOT NULL) w
               WHERE CONCAT_WS('_', worker, (SELECT x->>p.category FROM categories), product) ILIKE ?;""",
            [dumps(categories), user_id, f"%%{escape(m.text)}%%"],
            as_dict=True,
        )
        logging.debug(rows)
        if rows and rows[0]:
            for row in rows:
                msg_text = f'{messages["seller"]}: [{unescape(row["worker"])}](tg://user?id={row["bot"]})'
                msg_text += " (%s**%s**)\n" % (row["transactions"], "\*" * round(row["quality"]))
                msg_text += f'{categories[row["category"]]} - {unescape(row["product"])} {row["price"]}/{row["tag"]}'
                markup = InlineKeyboardMarkup(
                    inline_keyboard=[
                        [InlineKeyboardButton(text=buttons["payment"], callback_data=f'transaction_{row["id"]}')]
                    ]
                )
                await bot.send_message(cid, msg_text, parse_mode=ParseMode.MARKDOWN, reply_markup=markup)
        else:
            await bot.send_message(cid, messages["error_result"])
    del cur


@router.callback_query()
async def callback_handler(c: CallbackQuery) -> None:
    cid = c.from_user.id
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    user_id = get_user_id(cur, cid)
    if user_id < 0:
        await request_contact(cid)
    else:
        if cid in steps:
            del steps[cid]
        if "transaction" in c.data:
            steps[cid] = c.data
            await bot.send_message(cid, messages["request_amount"])
        # elif 'note' in c.data:
        #   steps[cid] = c.data
        #   await bot.send_message(cid, messages['request_note'])
        elif "grade" in c.data:
            steps[cid] = c.data
            await bot.send_message(cid, messages["request_grade"])
        elif "confirm" in c.data:
            tx_id = sub("confirm_", "", c.data)
            user_name = (cur.get("SELECT worker FROM workers WHERE id=?;", [user_id], one=True))[0]
            other = cur.get(
                "SELECT worker, bot FROM workers WHERE id=(SELECT client_id FROM transactions WHERE id=?);",
                [tx_id],
                one=True,
                as_dict=True,
            )
            row = cur.get(
                """SELECT admitted, cancelled_worker, cancelled_client, worker_id, client_id
                   FROM transactions t,
                   LATERAL(SELECT assurance, income, outcome FROM workers WHERE id=t.client_id) w
                   WHERE id=? AND (assurance * -1 <= income - outcome - total OR admitted);""",
                [tx_id],
                one=True,
                as_dict=True,
            )
            if row and not row["admitted"] and not row["cancelled_worker"] and not row["cancelled_client"]:
                cur.set("UPDATE transactions SET admitted=TRUE WHERE id=?", [tx_id])
                set_user_resumen(cur, row["worker_id"])
                set_user_resumen(cur, row["client_id"])
            if row:
                if not row["admitted"] and not row["cancelled_worker"] and not row["cancelled_client"]:
                    msg_text = f'{messages["status_admitted"]}\n\n{messages["status_balance_get"]}\n\n'
                    self_text = (
                        msg_text + f'{messages["client"]}: [{unescape(other["worker"])}](tg://user?id={other["bot"]})!'
                    )
                    other_text = msg_text + f'{messages["seller"]}: [{unescape(user_name)}](tg://user?id={cid})!'
                    # self_markup = InlineKeyboardMarkup(
                    #    inline_keyboard=[[InlineKeyboardButton(text=buttons["note"], callback_data=f"note_{tx_id}")]]
                    # )
                    await bot.send_message(cid, self_text, parse_mode=ParseMode.MARKDOWN, reply_markup=None)
                    await bot.answer_callback_query(c.id, messages["status_admitted"])
                    other_markup = InlineKeyboardMarkup(
                        inline_keyboard=[
                            [InlineKeyboardButton(text=buttons["grade"], callback_data=f"grade_{tx_id}")],
                        ]
                    )
                    await bot.send_message(
                        other["bot"], other_text, parse_mode=ParseMode.MARKDOWN, reply_markup=other_markup
                    )
                elif not row["cancelled_worker"] and not row["cancelled_client"]:
                    await bot.send_message(cid, messages["status_admitted_done"])
                    await bot.answer_callback_query(c.id, messages["status_admitted_done"])
                else:
                    await bot.send_message(cid, messages["status_cancelled"])
                    await bot.answer_callback_query(c.id, messages["status_cancelled_done"])
            else:
                msg_text = f'{messages["status_rejected"]}\n\n{messages["error_amount"]}\n\n'
                self_text = (
                    msg_text + f'{messages["client"]}: [{unescape(other["worker"])}](tg://user?id={other["bot"]})!'
                )
                other_text = msg_text + f'{messages["seller"]}: [{unescape(user_name)}](tg://user?id={cid})!'
                await bot.send_message(cid, self_text, parse_mode=ParseMode.MARKDOWN)
                await bot.answer_callback_query(c.id, messages["status_rejected"])
                await bot.send_message(other["bot"], other_text, parse_mode=ParseMode.MARKDOWN)
        elif "cancel" in c.data:
            tx_id = sub("cancel_", "", c.data)
            row = cur.get(
                """SELECT cancelled_worker, cancelled_client, worker_id,
                          (SELECT bot FROM workers WHERE id=t.worker_id) AS worker_bot,
                          (SELECT bot FROM workers WHERE id=t.client_id) AS client_bot
                   FROM transactions t WHERE id=? AND NOT admitted;""",
                [tx_id],
                one=True,
                as_dict=True,
            )
            if row and not row["cancelled_worker"] and not row["cancelled_client"]:
                if user_id == row["worker_id"]:
                    cur.set("UPDATE transactions SET cancelled_worker=TRUE WHERE id=?", [tx_id])
                else:
                    cur.set("UPDATE transactions SET cancelled_client=TRUE WHERE id=?", [tx_id])
            if row and not row["cancelled_worker"] and not row["cancelled_client"]:
                if user_id == row["worker_id"]:
                    await bot.send_message(
                        row["client_bot"], f'{messages["status_cancelled"]} -> {messages["seller"]}!'
                    )
                else:
                    await bot.send_message(
                        row["worker_bot"], f'{messages["status_cancelled"]} -> {messages["client"]}!'
                    )
                await bot.send_message(cid, messages["status_cancelled"])
                await bot.answer_callback_query(c.id, messages["status_cancelled"])
            elif row:
                await bot.send_message(cid, messages["status_cancelled_done"])
                await bot.answer_callback_query(c.id, messages["status_cancelled_done"])
            else:
                await bot.send_message(cid, messages["status_admitted_done"])
                await bot.answer_callback_query(c.id, messages["status_admitted_done"])
    del cur


async def main() -> None:
    await dp.start_polling(bot)


if __name__ == "__main__":
    run(main())
