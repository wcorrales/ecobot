from __future__ import annotations

import logging
from datetime import datetime
from duckdb import connect
from duckdb_cursor import CURSOR

from rich import box
from rich.table import Table
from rich.text import Text
from textual import events
from textual.app import App, ComposeResult
from textual.binding import Binding
from textual.containers import Container, Horizontal
from textual.widgets import Button, Footer, Header, Input, Static  # Switch

from config import AppConfig
from dicts import categories
from utils import escape, unescape, thous, set_logging


set_logging(AppConfig.LOG_LEVEL, log_file=f"{AppConfig.APP_PATH}ecobot.log")

if AppConfig.BOT_TOKEN == "":
    # TODO alert ERROR
    logging.error("BOT_TOKEN in config.py not updated!")


def set_db():
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    t = cur.get("SHOW TABLES;")
    if len(t) < 1:
        # TODO confirm
        with open(f"{AppConfig.APP_PATH}db.sql", "r") as f:
            cur.set(f.read())
    del cur


# set_db()


def get_balance():
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    rows = cur.get(
        """SELECT worker,
                  (SELECT COUNT(*) FROM products WHERE worker_id=w.id),
                  transactions, assurance,
                  CASE WHEN balance > 0 THEN balance ELSE 0 END AS incomes,
                  CASE WHEN balance < 0 THEN balance ELSE 0 END AS outcomes,
                  quality
           FROM workers w,
           LATERAL(SELECT income - outcome AS balance) x
           ORDER BY worker"""
    )
    del cur

    table_a = Table(show_edge=False, expand=True, row_styles=["none", "dim"], box=box.SIMPLE)
    table_a.add_column(Text.from_markup("[green]Users"), style="green")
    table_a.add_column(Text.from_markup("[blue]Products"), style="blue", justify="right")
    table_a.add_column(Text.from_markup("[blue]Transactions"), style="blue", justify="right")
    table_a.add_column(Text.from_markup("[blue]Assurance"), style="blue", justify="right", no_wrap=True)
    table_a.add_column(Text.from_markup("[red]Positive Balance"), style="red", justify="right", no_wrap=True)
    table_a.add_column(Text.from_markup("[red]Negative Balance"), style="red", justify="right", no_wrap=True)
    table_a.add_column(Text.from_markup("[magenta]Quality"), style="magenta", justify="right")
    totals = [len(rows), 0, 0, 0, 0]
    for row in map(list, rows):
        totals[1] = totals[1] + row[1]
        totals[2] = totals[2] + row[2]
        totals[3] = round(totals[3] + row[4], 2)
        totals[4] = round(totals[4] + row[5], 2)
        row[0] = unescape(row[0])
        row[3] = thous(round(row[3]))
        row[4] = thous(round(row[4], 2))
        row[5] = thous(round(row[5], 2))
        row[6] = round(row[6] or 0, 1)
        table_a.add_row(*map(str, row))
    totals[2] = int(totals[2] / 2)

    table_b = Table(show_edge=False, expand=True, box=box.SIMPLE)
    table_b.add_column(Text.from_markup("[green]Users"), style="green")
    table_b.add_column(Text.from_markup("[blue]Products"), style="blue", justify="right")
    table_b.add_column(Text.from_markup("[blue]Transactions"), style="blue", justify="right")
    table_b.add_column(Text.from_markup("[red]Positive Balance"), style="red", justify="right", no_wrap=True)
    table_b.add_column(Text.from_markup("[red]Negative Balance"), style="red", justify="right", no_wrap=True)
    table_b.add_row(*map(thous, totals))

    return (table_a, table_b)


def get_users():
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    rows = cur.get(
        """SELECT id, worker, assurance, phone, address, created
           FROM workers
           ORDER BY worker"""
    )
    del cur

    table = Table(show_edge=False, expand=True, row_styles=["none", "dim"], box=box.SIMPLE)
    table.add_column(Text.from_markup("[green]Id"), style="green", no_wrap=True)
    table.add_column(Text.from_markup("[green]User"), style="green", no_wrap=True)
    table.add_column(Text.from_markup("[blue]Assurance"), style="blue", justify="right", no_wrap=True)
    table.add_column(Text.from_markup("[red]Phone"), style="red", justify="right", no_wrap=True)
    table.add_column(Text.from_markup("[magenta]Address"), style="magenta", justify="right")
    table.add_column(Text.from_markup("[magenta]Created"), style="magenta", justify="right")
    for row in map(list, rows):
        row[1] = unescape(row[1])
        row[2] = thous(round(row[2], 2))
        row[4] = unescape(row[4])
        table.add_row(*map(str, row))

    return table


def set_users(item_search, item_input):
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    if item_search.has_focus:
        row = cur.get(
            "SELECT id, worker, assurance, phone, address FROM workers WHERE id=?",
            (item_search.value,),
            one=True,
        )
        if not row:
            raise Exception("User not exists!")
        user = [f"{r or ''}".strip() for r in row]
        user[1] = unescape(user[1])
        user[4] = unescape(user[4])
        item_input.value = ",".join(user)
        item_input.focus()
    else:
        data = [x.strip() for x in item_input.value.split(",")]
        if len(data) > 1:
            worker = cur.get("SELECT id FROM workers WHERE worker=?", (escape(data[1]),), one=True)
            if worker and (data[0] == "0" or data[0] != str(worker[0])):
                raise Exception("User exists!")

            phone = cur.get("SELECT id FROM workers WHERE phone=?", (data[3],), one=True)
            if phone and (phone[0] == "0" or data[0] != str(phone[0])):
                raise Exception("Phone exists!")

            if data[0] == "0":
                cur.set(
                    """INSERT INTO workers
                                   VALUES(NEXTVAL('workers_id_seq'), ?, ?, NULL, ?, NULL, NULL, NULL,
                                          ?, 0, 0, 0, NULL, NULL, ?, NULL, NULL);""",
                    (
                        escape(data[1]),
                        data[3],
                        escape(data[4]),
                        data[2],
                        datetime.now(AppConfig.TIMEZONE).strftime("%Y-%m-%d %H:%M:%S.%f"),
                    ),
                )
                raise Exception("INSERT OK!")
            else:
                cur.set(
                    "UPDATE workers SET worker=?, assurance=?, phone=?, address=? WHERE id=?",
                    (escape(data[1]), data[2], data[3], escape(data[4]), data[0]),
                )
                raise Exception("UPDATE OK!")
        else:
            cur.set("DELETE FROM workers WHERE id=?", (data[0],))
            raise Exception("DELETE OK!")
    del cur


def get_products():
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    rows = cur.get(
        """SELECT id, product, category, tag, price, worker_id,
                  (SELECT worker FROM workers WHERE id=p.worker_id)
           FROM products p
           ORDER BY product"""
    )
    del cur

    table = Table(show_edge=False, expand=True, row_styles=["none", "dim"], box=box.SIMPLE)
    table.add_column(Text.from_markup("[green]Id"), style="green", no_wrap=True)
    table.add_column(Text.from_markup("[green]Product"), style="green")
    table.add_column(Text.from_markup("[blue]Category"), style="blue")
    table.add_column(Text.from_markup("[red]Tag"), style="red", justify="right")
    table.add_column(Text.from_markup("[red]Price"), style="red", justify="right", no_wrap=True)
    table.add_column(Text.from_markup("[magenta]U_id"), style="magenta", justify="right")
    table.add_column(Text.from_markup("[magenta]User"), style="magenta")
    for row in map(list, rows):
        row[1] = unescape(row[1])
        row[2] = categories[row[2]]
        row[4] = thous(round(row[4], 2))
        row[6] = unescape(row[6])
        table.add_row(*map(str, row))

    return table


def set_products(item_search, item_input):
    cur = CURSOR(connect(f"{AppConfig.APP_PATH}ecobot.duckdb"))
    if item_search.has_focus:
        row = cur.get(
            "SELECT id, product, category, tag, price, worker_id FROM products p WHERE id=?",
            (item_search.value,),
            one=True,
        )
        if not row:
            raise Exception("Product not exists!")
        product = [f"{r or ''}".strip() for r in row]
        product[1] = unescape(product[1])
        item_input.value = ",".join(product)
        item_input.focus()
    else:
        data = [x.strip() for x in item_input.value.split(",")]
        if len(data) > 1:
            # product = cur.get("SELECT id FROM products WHERE product=?", (escape(data[1]),), one=True)
            # if product and (data[0] == "0" or data[0] != str(product[0])):
            #    raise Exception("Product exists!")

            if data[2] not in categories.keys():
                raise Exception("Category not exists!")

            if data[0] == "0":
                cur.set(
                    "INSERT INTO products VALUES(NEXTVAL('products_id_seq'), ?, ?, ?, ?, ?, ?);",
                    (
                        data[5],
                        data[2],
                        escape(data[1]),
                        data[3],
                        data[4],
                        datetime.now(AppConfig.TIMEZONE).strftime("%Y-%m-%d %H:%M:%S.%f"),
                    ),
                )
                raise Exception("INSERT OK!")
            else:
                # "UPDATE products SET product=?, category=?, tag=?, price=?, worker_id=? WHERE id=?",
                # (escape(data[1]), data[2], data[3], data[4], data[5], data[0]),
                worker_id = cur.get("SELECT worker_id FROM products WHERE id=?", (data[0],), one=True)
                if data[5] != str(worker_id[0]):
                    raise Exception("If need to update worker_id: delete -> insert!")
                cur.set(
                    "UPDATE products SET product=?, category=?, tag=?, price=? WHERE id=?",
                    (escape(data[1]), data[2], data[3], data[4], data[0]),
                )
                raise Exception("UPDATE OK!")
        else:
            cur.set("DELETE FROM products WHERE id=?", (data[0],))
            raise Exception("DELETE OK!")
    del cur


# transactions: Miembro, Cliente, Fecha, Categoria, Producto, Total, Aceptado, Calidad


class Nav(Container):
    def compose(self) -> ComposeResult:
        btns = (
            "[@click=app.set_main('balance')]Balance[/]",
            "[@click=app.set_main('users')]Users[/]",
            "[@click=app.set_main('products')]Products[/]",
        )
        yield Static("              ".join(btns))


class Main(Container):
    def compose(self) -> ComposeResult:
        t_a, t_b = get_balance()
        yield Static("Balance", id="title", classes="subtitle")
        yield Static(t_b, id="table0", classes="table pad")
        yield Static(t_a, id="table1", classes="table pad")


class Top(Container):
    item: str = None

    # def __init__(self, item: str = None, *args, **kargs) -> None:
    #    super().__init__(*args, **kargs)
    #    self.item = item

    def compose(self) -> ComposeResult:
        yield Static("[@click=app.close_top()]close[/]", classes="close")
        yield Static("", id="item_title", classes="subtitle")
        yield Horizontal(
            Input(id="item_search", placeholder="search id . . ."),
            Input(id="item_input"),
        )
        yield Static("", id="item_error")

    def on_key(self, event: events.Key) -> None:
        if event.key == "enter":  # tab
            try:
                item_search = self.query_one("#item_search", Input)
                item_input = self.query_one("#item_input", Input)
                if self.item == "users":
                    set_users(item_search, item_input)
                else:
                    set_products(item_search, item_input)
            except Exception as e:
                self.query_one("#item_error", Static).update(str(e))


class EcoBot(App):
    CSS_PATH = "admin.css"
    TITLE = "EcoBot"
    BINDINGS = [
        ("ctrl+t", "app.toggle_dark", "Toggle Dark mode"),
        ("ctrl+s", "app.screenshot()", "Screenshot"),
        Binding("ctrl+c,ctrl+q", "app.quit", "Quit", show=True),
    ]

    def compose(self) -> ComposeResult:
        yield Container(
            Top(id="top", classes="-hidden"),
            Header(show_clock=False),
            Nav(),
            Main(),
        )
        yield Footer()

    def action_set_main(self, query: str) -> None:
        logging.debug(f"set_main: {query}")
        if query == "balance":
            t_a, t_b = get_balance()
            self.query_one("#title", Static).update("Balance")
            self.query_one("#table0", Static).update(t_b)
            self.query_one("#table1", Static).update(t_a)
        if query in ("users", "products"):
            if query == "users":
                item_categories = ""
                item_placeholder = "id,user,assurance,phone,direction"
            else:
                item_categories = f"\n(categories: {','.join(categories.keys())})"
                item_placeholder = "id,product,category,tag,price,user_id"
            self.query_one("#title", Static).update(
                f"{query.capitalize()}   [@click=app.toggle_class('Top', '-hidden')]SET ITEM[/]"
            )
            self.query_one("#table0", Static).update(get_users() if query == "users" else get_products())
            self.query_one("#table1", Static).update("")
            self.query_one("#top", Container).item = query
            self.query_one("#item_title", Static).update(
                f"SET ITEM (insert: id = 0, update: id = #, delete: only id){item_categories}"
            )
            self.query_one("#item_search", Input).focus()
            self.query_one("#item_input", Input).value = ""
            self.query_one("#item_input", Input).placeholder = item_placeholder
            self.query_one("#item_error", Static).update("")

    def action_close_top(self) -> None:
        logging.debug("close_top")
        top = self.query_one("#top", Container)
        top.toggle_class("Top", "-hidden")
        self.query_one("#table0", Static).update(get_users() if top.item == "users" else get_products())
        self.query_one("#item_search", Input).focus()
        self.query_one("#item_input", Input).value = ""
        self.query_one("#item_error", Static).update("")

    def action_screenshot(self, filename: str | None = None, path: str = "./") -> None:
        logging.debug("screenshot")
        self.bell()
        path = self.save_screenshot(filename, path)
        message = Text.assemble("Screenshot saved to ", (f"'{path}'", "bold green"))


app = EcoBot()
if __name__ == "__main__":
    app.run()
