# coding=utf-8
import logging

from datetime import datetime
from io import BytesIO
from re import sub
from reportlab.lib.units import inch
from reportlab.lib.pagesizes import A4
from reportlab.pdfgen import canvas
from rich.box import Box
from rich.console import Console
from rich.table import Table

from config import AppConfig


def escape(s):
    return sub(r"([áéíóúüñÁÉÍÓÚÜÑ])", lambda x: "&#%s;" % ord(x.group()), s or "")


def unescape(s):
    return sub(r"(&#\d{3};)", lambda x: chr(int(x.group()[2:-1])), s or "")


def thous(x):
    return sub(r"(\d{3})(?=\d)", r"\1,", str(x)[::-1])[::-1]


def set_table(header, body, foot=None, justify=None, widths=None):
    ASCII_SIMPLE = Box(
        """\
+--+
    
|==|
    
|-+|
|--|
    
+--+
""",
        ascii=True,
    )

    t = Table(
        box=ASCII_SIMPLE,  # box.ASCII
        header_style="none",
        footer_style="none",
        show_edge=False,
        show_footer=bool(foot),
    )

    for i in range(len(header)):
        t.add_column(
            header[i],
            str(foot[i]) if foot else None,
            justify={"r": "right", "l": "left"}[justify[i]] if justify else None,
            width=widths[i] if widths else None,
        )

    for b in body:
        t.add_row(*map(str, b))

    c = Console(width=87)
    with c.capture() as capture:
        c.print(t)

    return capture.get()


def set_pdf(title, content, font_size=10):
    top_margin = A4[1] - inch
    bottom_margin = inch / 2
    left_margin = inch / 2
    right_margin = A4[0] - inch / 2
    # frame_width = right_margin - left_margin

    pdf_buffer = BytesIO()
    canv = canvas.Canvas(pdf_buffer)
    canv.setTitle(f"EcoBot - {title}")
    canv.drawInlineImage("logo_vaca.jpg", 470, 710)
    canv.drawCentredString(300, 770, "EcoBot")
    # canv.setFillColorRGB(0, 0, 255)
    canv.setFont("Courier-Bold", 24)
    canv.drawCentredString(290, 720, title)
    canv.line(left_margin, 710, right_margin, 710)

    text = canv.beginText(left_margin, 680)
    text.setFont("Courier", font_size)
    lines = content.split("\n")
    for line in lines:
        text.textLine(line)
        y = text.getY()
        if y < bottom_margin:
            canv.drawText(text)
            canv.showPage()
            text = canv.beginText(left_margin, top_margin)
            text.setFont("Courier", font_size)
            text.textLine(lines[0])
            text.textLine(lines[1])

    if text:
        canv.drawText(text)
        canv.drawString(
            right_margin - inch * 2.75,
            bottom_margin - inch * 0.25,
            f'Fecha (hora): {datetime.now(AppConfig.TIMEZONE).strftime("%Y-%m-%d (%H:%M:%S)")}',
        )
        canv.showPage()
    else:
        raise Exception("Error en pdf!")

    canv.save()
    pdf = pdf_buffer.getvalue()
    pdf_buffer.close()
    return pdf


class LogFormatter(logging.Formatter):
    grey = "\x1b[38;20m"
    green = "\x1b[32;20m"
    yellow = "\x1b[33;20m"
    red = "\x1b[31;20m"
    bold_red = "\x1b[31;1m"
    reset = "\x1b[0m"
    format = "%(levelname)s:%(name)s:%(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.INFO: grey + format + reset,
        logging.DEBUG: green + format + reset,
        logging.WARNING: yellow + format + reset,
        logging.ERROR: red + format + reset,
        logging.CRITICAL: bold_red + format + reset,
    }

    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)


def set_logging(logging_level, log_file=""):
    logger = logging.getLogger()
    logger.setLevel(logging_level)
    if log_file:
        handler = logging.FileHandler(filename=log_file)
    else:
        handler = logging.StreamHandler()
    handler.setFormatter(LogFormatter())
    logger.addHandler(handler)
