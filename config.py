import logging
from zoneinfo import ZoneInfo


class AppConfig(object):
    APP_PATH = ""  # "/srv/ecobot/"
    BOT_TOKEN = "token-xxx"
    LOG_LEVEL = logging.WARNING  # DEBUG, WARNING, ERROR
    TIMEZONE = ZoneInfo("America/Costa_Rica")  # datetime.now(tz)
