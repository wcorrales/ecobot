MUTUAL CREDIT Network powered by Python, DuckDB, Aiogram and Textual.
---------------------------------------------------------------

![EcoBot - python, duckdb, aiogram, textual](/ecobot.jpg?raw=true "EcoBot example")

1. Create Telegram Bot and edit config.py

2. Install Python library
```
pip3 install -r requirements.txt
```

3. Manage Users and Products
```
python3 admin.py
```

4. Run digital Wallet
```
python3 bot.py
```

5. For production create systemd service

nano /etc/systemd/system/ecobot.service
```
[Unit]
Description = EcoBot
After = network.target

[Service]
PermissionsStartOnly = true
PIDFile = /run/ecobot/ecobot.pid
User = root
Group = root
WorkingDirectory = /srv/ecobot
Environment = "PATH=/srv/ecobot/venv/bin"
ExecStartPre = /bin/mkdir /run/ecobot
ExecStartPre = /bin/chown -R root:root /run/ecobot
ExecStart = /srv/ecobot/venv/bin/python bot.py --pid /run/ecobot/ecobot.pid
ExecReload = /bin/kill -s HUP $MAINPID
ExecStop = /bin/kill -s TERM $MAINPID
ExecStopPost = /bin/rm -rf /run/ecobot
PrivateTmp = true

[Install]
WantedBy = multi-user.target
```

```
chmod 755 /etc/systemd/system/ecobot.service
systemctl daemon-reload
systemctl enable ecobot.service
systemctl start ecobot.service
systemctl status ecobot.service
```