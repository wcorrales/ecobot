-- ************************************** --
------------------ TABLES ------------------
-- ************************************** --
CREATE TABLE IF NOT EXISTS workers (
  id INTEGER NOT NULL,  -- PRIMARY KEY
  worker VARCHAR(150) NOT NULL,  -- UNIQUE
  phone VARCHAR(20) NOT NULL,  -- UNIQUE
  email VARCHAR(60) DEFAULT NULL,  -- UNIQUE
  address VARCHAR(250) DEFAULT NULL,
  lat DOUBLE PRECISION DEFAULT NULL,
  lon DOUBLE PRECISION DEFAULT NULL,
  bot BIGINT DEFAULT NULL,

  assurance NUMERIC NOT NULL,  -- max limit credit
  income NUMERIC NOT NULL,
  outcome NUMERIC NOT NULL,
  transactions INTEGER NOT NULL,
  service NUMERIC DEFAULT NULL,
  quality NUMERIC DEFAULT NULL,

  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  logged TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  blocked TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,

  CHECK (TRIM(worker) <> '' AND worker !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  CHECK (TRIM(email) <> '' AND email ~ '\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*'),
  CHECK (TRIM(phone) <> '' AND phone !~ '[^0-9+-]+'),
  CHECK (TRIM(address) <> '' AND address !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+')
);

CREATE SEQUENCE workers_id_seq;


CREATE TABLE IF NOT EXISTS products (
  id INTEGER PRIMARY KEY NOT NULL,
  worker_id INTEGER NOT NULL,
  category VARCHAR(15) NOT NULL,
  product VARCHAR(100) NOT NULL,
  tag VARCHAR(10) NOT NULL,
  price NUMERIC NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  CHECK (category IN ('food', 'drink', 'plant', 'input', 'home', 'personal', 'service', 'other')),
  CHECK (TRIM(product) <> '' AND product !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  CHECK (TRIM(tag) <> '' AND tag !~ '[^a-z0-9-]+'),
  CHECK (price >= 0)
--  FOREIGN KEY(worker_id) REFERENCES workers(id)
);
-- ALTER TABLE products ADD CONSTRAINT products_worker_id_fk FOREIGN KEY (worker_id) REFERENCES workers(id);

CREATE SEQUENCE products_id_seq;


CREATE TABLE IF NOT EXISTS transactions (
  id INTEGER PRIMARY KEY NOT NULL,
  worker_id INTEGER NOT NULL,
  client_id INTEGER NOT NULL,
  category VARCHAR(10) NOT NULL,
  product VARCHAR(100) NOT NULL,
--  tag VARCHAR(15) NOT NULL,
--  price NUMERIC NOT NULL,
  total NUMERIC NOT NULL,
  admitted BOOLEAN NOT NULL,
  cancelled_worker BOOLEAN NOT NULL,
  cancelled_client BOOLEAN NOT NULL,
  priority INTEGER NOT NULL,  -- default 1 {1:'low', 2:'normal', 3:'high'}
  note VARCHAR(150) DEFAULT NULL,
  service INTEGER DEFAULT NULL,  -- 0-5
  quality INTEGER DEFAULT NULL,  -- 0-5
  comment VARCHAR(150) DEFAULT NULL,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,  -- default NOW()
  scheduled TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  scheduled_start DATE DEFAULT NULL,
  scheduled_stop DATE DEFAULT NULL,
  scheduled_title VARCHAR(150) DEFAULT NULL,
  scheduled_color VARCHAR(10) DEFAULT NULL,
  delivered TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  archived TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  cancelled TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  cancelled_category VARCHAR(15) DEFAULT NULL,
  cancelled_comment VARCHAR(150) DEFAULT NULL,
  CHECK (TRIM(product) <> '' AND product !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
--  CHECK (price >= 0),
  CHECK (total >= 0),
  CHECK (priority in (1, 2, 3)),
  CHECK (TRIM(note) <> '' AND note !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  CHECK (TRIM(comment) <> '' AND comment !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  CHECK (TRIM(scheduled_title) <> '' AND scheduled_title !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  CHECK (TRIM(scheduled_color) <> '' AND scheduled_color !~ '[^a-z0-9#]+'),
  CHECK (cancelled_category in ('not_friend', 'not_touch', 'client_view', 'inventory', 'pick_up', 'delivery', 'devolution')),
  CHECK (TRIM(cancelled_comment) <> '' AND cancelled_comment !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+')
--  FOREIGN KEY(worker_id) REFERENCES workers(id),
--  FOREIGN KEY(client_id) REFERENCES workers(id)
);

-- ALTER TABLE transactions ADD CONSTRAINT transactions_worker_id_fk FOREIGN KEY(worker_id) REFERENCES workers(id);
-- ALTER TABLE transactions ADD CONSTRAINT transactions_client_id_fk FOREIGN KEY(client_id) REFERENCES workers(id);

CREATE SEQUENCE transactions_id_seq;

-- CREATE INDEX transactions_worker_id_idx ON transactions(worker_id) WHERE admitted IS TRUE;
-- CREATE INDEX transactions_client_id_idx ON transactions(client_id) WHERE admitted IS TRUE;


-- ************************************* --
------------------ VIEWS ------------------
-- ************************************* --


-- ************************************** --
------------------ QUERYS ------------------
-- ************************************** --

/*
WITH balances AS (SELECT worker, transactions, balance
                  FROM workers,
                  LATERAL(SELECT income - outcome AS balance) x
                  WHERE balance <> 0)
SELECT worker, transactions,
       CASE WHEN balance > 0 THEN balance ELSE 0 END AS incomes,
       CASE WHEN balance < 0 THEN balance ELSE 0 END AS outcomes
FROM balances;
*/


-- **************************************** --
------------------ FUNCTIONS -----------------
-- **************************************** --


-- **************************************** --
------------------ TRIGGERS ------------------
-- **************************************** --
